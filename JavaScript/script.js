window.onload = function() {
  var slider = document.getElementsByClassName('slider')[0],
      images = 4,
      counter =  1,
      nextBtn = slider.getElementsByClassName('slider-right-arrow')[0],
      prevBtn = slider.getElementsByClassName('slider-left-arrow')[0];

      function showImage(index)
      {
        //for(var i = 0; i < images.length; ++i)
        //  images[i].className = 'hideImage';
        //images[index].className = 'slider-image'
        slider.style.backgroundImage = "url('../img/slider/0"+index+".jpg')"
      }

      function nextImg () 
      {
        if (counter == images) 
          counter = 0;
        counter++;
        showImage(counter);
      }
  
  function prevImg () 
  {
    if (counter == 1)
      counter = images;
    counter--;
    showImage(counter);
  }
  
//  If you want to have an auto-slider, uncomment this: 
var myTimer = window.setInterval(prevImg, 5000);
  
  // give the buttons an onclick event
   nextBtn.onclick = function(){
    nextImg();
    clearInterval(myTimer);
    myTimer = window.setInterval(prevImg, 5000);
  }
   prevBtn.onclick = function(){
    prevImg();
    clearInterval(myTimer);
    myTimer = window.setInterval(prevImg, 5000);
  }
  // start it
  showImage(counter);

}