module.exports = function(grunt){
	grunt.initConfig({
		jade : {
			compile : {
				options : {
					pretty: true,
				},
					
				files : {
					'html/index.html' : ['jade/index.jade']
				}
			}
		},
		watch : {
			grunt : {files : ['Gruntfile.js']},
			jade : {
				files : ['jade/*.jade'],
				tasks : ['jade']
			},
			stylus : {
				files : ['style/style.styl'],
				tasks : ['stylus']
			},
			autoprefixer:{
				files:['css/style.css'],
				tasks:['autoprefixer']
			}
		},
		stylus : {
			compile : {
				options : {
					compress : false
				},
				files: {
					'css/style.css' : 'style/style.styl'
				}
			}
		},
		autoprefixer :{
			options:{
				browsers: ['last 2 versions', 'ie 8', 'ie 9', 'Firefox >= 18', 'ios_saf >= 7']
			},
			main: {
				expand: true,
				flatten: true,
				src: 'css/style.css',
				dest: 'css'
			}
		}
	});

	grunt.loadNpmTasks('grunt-autoprefixer');
	grunt.loadNpmTasks('grunt-contrib-jade');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-stylus');
	grunt.registerTask('build', 'Convert jade template to html', ['jade', 'stylus', 'autoprefixer', 'watch']);
};